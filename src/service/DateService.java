package service;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class DateService {
    public long getNumberOfDaysBetweenDates(String date1, String date2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate localDate1 = LocalDate.parse(date1, formatter);
        LocalDate localDate2 = LocalDate.parse(date2, formatter);
        return Math.abs(ChronoUnit.DAYS.between(localDate1, localDate2));
    }

    public void showMyAge(LocalDateTime dateTimeOfBirth) {
        LocalDateTime dateTimeNow = LocalDateTime.now();
        LocalDateTime tempDateTime = LocalDateTime.from(dateTimeOfBirth);

        long years = tempDateTime.until(dateTimeNow, ChronoUnit.YEARS);
        tempDateTime = tempDateTime.plusYears(years);

        long months = tempDateTime.until(dateTimeNow, ChronoUnit.MONTHS);
        tempDateTime = tempDateTime.plusMonths(months);

        long days = tempDateTime.until(dateTimeNow, ChronoUnit.DAYS);
        tempDateTime = tempDateTime.plusDays(days);

        long hours = tempDateTime.until(dateTimeNow, ChronoUnit.HOURS);
        tempDateTime = tempDateTime.plusHours(hours);

        long minutes = tempDateTime.until(dateTimeNow, ChronoUnit.MINUTES);
        tempDateTime = tempDateTime.plusMinutes(minutes);

        long seconds = tempDateTime.until(dateTimeNow, ChronoUnit.SECONDS);
        tempDateTime = tempDateTime.plusSeconds(seconds);

        System.out.println("My Age: " + years + " years " + months + " months " + days + " days " +
                hours + " hours " + minutes + " minutes " + seconds + " seconds.");
    }

    public String convertDate(String strDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE',' MMM d',' yyyy hh':'mm':'ss a", Locale.US);
        LocalDateTime localDate = LocalDateTime.parse(strDate, formatter);
        return localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public String convertZonedDate(String strDate) {
        ZoneId zoneId = ZoneId.of("Europe/Samara");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(strDate).withZoneSameInstant(zoneId);
        return zonedDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }
}
