import demo.DemoService;
import service.DateService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new DateService()).execute();
    }
}
