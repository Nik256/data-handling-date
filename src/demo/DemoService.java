package demo;

import service.DateService;

import java.time.LocalDateTime;

public class DemoService {
    private DateService dateService;

    public DemoService(DateService dateService) {
        this.dateService = dateService;
    }

    public void execute() {
        LocalDateTime dateTimeOfBirth = LocalDateTime.of(1995, 9, 7, 2, 22, 22);
        dateService.showMyAge(dateTimeOfBirth);

        String strDate1 = "11.03.1954";
        String strDate2 = "30.06.1978";
        System.out.println(dateService.getNumberOfDaysBetweenDates(strDate1, strDate2) + " days between: " +
                strDate1 + " and " + strDate2);

        String strDate3 = "Wednesday, Aug 10, 2016 12:10:56 PM";
        System.out.println(strDate3 + " -> " + dateService.convertDate(strDate3));

        String strDate4 = "2016-08-16T10:15:30+08:00";
        System.out.println(strDate4 + " converted to Izhevsk local time: " + dateService.convertZonedDate(strDate4));
    }
}
